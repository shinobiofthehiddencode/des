#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <getopt.h>
#include "DES.h"

/* Specifying the expected options */
struct option long_options[] = {
    {"encrypt", no_argument, 0, 'e'},
    {"decrypt", no_argument, 0, 'd'},
    {NULL, 0, NULL, 0}
};

int main(int argc, char **argv){

    /* Print start notifications */
    printf("\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
    printf("\nThis is an implementation of DES (Data Encryption Standard).");
    printf("\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n\n");

    char *key = calloc(sizeof(char), 9);

    /* Opening files for reading message and key. Filenames are supplied as the command line arguments 
        with '-m' flag for file having the message and -k for file having the key . Use -g flag to generate file having key. */
    int arg, prev_ind, index_ptr=0;
    operation_mode mode;
    FILE *file, *output_file;
    while(prev_ind = optind, (arg = getopt_long(argc, argv, "g:m:k:e::d::", long_options, &index_ptr)) != -1){
        if ( optind == prev_ind + 2 && *optarg == '-' ) {
            arg = ':';
            -- optind;
        }
        switch (arg){
            case 'e':
                mode = encryption; // Encrypt
                break;
            case 'd':
                mode = decryption; // Decrypt
                break;
            case 'g':
                if (!(file = fopen(strcat(optarg,".key"), "w"))) {
                    perror(optarg);
                    exit (1);
                }
                generate_key(file);
                fclose(file);
                break;
            case 'k':
                if (!(file = fopen(optarg, "r"))) {
                    perror(optarg);
                    abort ();
                }
                key = read_key_from_file(file);
                fclose(file);
                break;
            case 'm':
                if (!(file = fopen(optarg, "r"))) {
                    perror(optarg);
                    abort ();
                }
                break;
            default:
                abort ();
        }
    }

    long int pos=0;
    size_t num = 0;
    size_t *number_of_chars_read = &num;
    int number_of_chars_to_be_read;
    char *key_64_bit = calloc(sizeof(char), BLOCK_SIZE+1);                        // Will be used for 64 bit key.
    key_64_bit = string_to_binary(key);
    key_64_bit[64] = '\0';

    /* Generate a set of 16 subkeys for each of the rounds */
    sub_key_set *sub_keys = calloc(sizeof(sub_key_set), 17);   
    generate_sub_keys(key_64_bit, sub_keys);

    /* Erase the contents of output file if it already exists */
    clear_contents(&output_file, mode);

    /* Loop through the file to read and encrypt/decrypt data */
    do{
        char *message;                                                                // Will be used for text message (human-readable form) from file.
        char *encrypted_message = calloc(sizeof(char), BLOCK_SIZE+1);                 // Will be used for 64 bit encrypted message.
        char *plain_text_message = calloc(sizeof(char), BLOCK_SIZE+1);                // Will be used for 64 bit plaintext message.
        plain_text_message[BLOCK_SIZE] = '\0';
        /* Initialization based on the mode */
        if (mode == encryption){
            message = calloc(sizeof(char), 9);
            message[8] = '\0';
            number_of_chars_to_be_read = 8;
        }else if (mode == decryption){
            message = calloc(sizeof(char), BLOCK_SIZE + 1);
            message[BLOCK_SIZE] = '\0';
            number_of_chars_to_be_read = BLOCK_SIZE;
        }

        /* Read data */
        pos = ftell(file);
        fseek(file, pos, SEEK_SET);
        *number_of_chars_read  = fread(message, 1, number_of_chars_to_be_read, file);

        /* Break out of the loop if file has reached its end */
        if(feof(file) && *number_of_chars_read == 0) break;

        if (mode == encryption){

            /* Start encryption */
            strncpy(plain_text_message, string_to_binary(pkcs5_padding(message)), BLOCK_SIZE);
            encrypt(encrypted_message, plain_text_message, sub_keys);

            /* Write the ciphertext to the output file */
            output_file = fopen(ENCRYPTION_OUTPUT_FILE, "a");
            write_to_file(&output_file, encrypted_message);
            fclose(output_file);
        }else if (mode == decryption){
            
            /* Start decryption */
            strncpy(encrypted_message, message, BLOCK_SIZE);
            decrypt(plain_text_message, encrypted_message, sub_keys);

            /* Write the plain text to the output file */ 
            output_file = fopen(DECRYPTION_OUTPUT_FILE, "a");
            write_to_file(&output_file, remove_padding_if_needed(binary_to_string(plain_text_message)));
            fclose(output_file);
        }
        /* Free allocated memory */
        free(plain_text_message);
        free(encrypted_message);
        free(message);
    }while((int)*number_of_chars_read == number_of_chars_to_be_read && !feof(file));
    
    /* Free allocated memory */
    free(sub_keys);
    free(key_64_bit);
    free(key);

    /* Close open files */
    fclose(file);

    /* Print the completion notification */
    if (mode == encryption){
        printf("\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        printf("\nMESSAGE ENCRYPTED!.");
        printf("\nThe encrypted message is stored in "ENCRYPTION_OUTPUT_FILE"");
        printf("\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n\n");
    }else if (mode == decryption){
        printf("\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        printf("\nMESSAGE DECRYPTED!.");
        printf("\nThe decrypted message is stored in "DECRYPTION_OUTPUT_FILE"");
        printf("\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n\n");
    }
    
    return 0;
} 
